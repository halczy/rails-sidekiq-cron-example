class GenRandNameJob < ApplicationJob
  queue_as :default

  def perform(*args)
    Post.create!(name: Faker::Dog.name)
  end
end

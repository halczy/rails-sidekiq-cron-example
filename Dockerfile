FROM ruby:2.5.1-alpine

RUN apk add --no-cache build-base \
                       nodejs \
                       sqlite-dev \
                       file imagemagick \
                       yarn \
                       tzdata \
                       git \
                       ffmpeg

RUN gem install bundler -s https://gems.ruby-china.org/ && \
    bundle config mirror.https://rubygems.org https://gems.ruby-china.org

RUN mkdir -p /usr/src/work_cycle
COPY Gemfile* /usr/src/work_cycle/
WORKDIR /usr/src/work_cycle/
RUN bundle install

COPY package.json /usr/src/work_cycle/
RUN yarn install

EXPOSE 3000

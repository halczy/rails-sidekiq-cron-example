require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  root 'posts#index'
  resources :posts
  mount Sidekiq::Web => '/sidekiq'
end
